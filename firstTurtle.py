import turtle 			#allow us to use turtles

wn=turtle.Screen() 		#creates a playground
alex=turtle.Turtle() 	#create a turtle, assign to alex

alex.forward(50)
alex.left(150)
alex.forward(30)

wn.mainloop() 			#wait for user to close window

